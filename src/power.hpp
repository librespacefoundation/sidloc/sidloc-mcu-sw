/*
 *  SIDLOC MCU software: SIDLOC beacon software
 *
 *  Copyright (C) 2022, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include "max17261.h"
#include <cstdint>

class power
{
public:
  static constexpr uint32_t battery_capacity_mAh = 1240;
  static constexpr uint32_t battery_v_empty      = (3300 / 10);
  static constexpr uint32_t battery_v_recovery   = (3880 / 40);
  static constexpr uint32_t charging_voltage     = 4350;
  static constexpr uint32_t charge_term_i        = 25;
  static constexpr uint16_t max17261_i2c_addr    = 0x36; // Use the 7-bit
  static constexpr uint32_t critical_v_thrh      = 3400;
  static constexpr uint32_t low_v_entry_thrh     = 3600;
  static constexpr uint32_t low_v_exit_thrh      = 3800;
  static constexpr int8_t   low_soc_entry_thrh   = 20;
  static constexpr int8_t   low_soc_exit_thrh    = 25;

  static power &
  get_instance()
  {
    static power instance;
    return instance;
  }

  enum class status : uint8_t
  {
    NORMAL,
    LOW,
    CRITICAL
  };

  /* Singleton */
  power(power const &) = delete;

  void
  operator=(power const &) = delete;

  void
  update();

  uint16_t
  voltage() const;

  uint16_t
  voltage_avg() const;

  uint16_t
  voltage_max() const;

  uint16_t
  voltage_min() const;

  int16_t
  current() const;

  int16_t
  current_avg() const;

  int16_t
  current_max() const;

  int16_t
  current_min() const;

  int8_t
  temperature_die() const;

  int8_t
  temperature() const;

  int8_t
  temperature_avg() const;

  int8_t
  temperature_max() const;

  int8_t
  temperature_min() const;

  uint16_t
  time_to_empty_mins() const;

  int16_t
  remaining_capacity_mAh() const;

  int16_t
  full_capacity_mAh() const;

  int8_t
  charging_percentage() const;

  void
  reset_minmax();

  status
  get_status() const;

private:
  struct max17261_conf m_max17261;
  uint16_t             m_voltage;
  uint16_t             m_voltage_avg;
  uint16_t             m_voltage_max;
  uint16_t             m_voltage_min;
  int16_t              m_current;
  int16_t              m_current_avg;
  int16_t              m_current_max;
  int16_t              m_current_min;
  int8_t               m_temperature_die;
  int8_t               m_temperature;
  int8_t               m_temperature_avg;
  int8_t               m_temperature_max;
  int8_t               m_temperature_min;
  uint16_t             m_tte;
  int16_t              m_remaining_capacity;
  int16_t              m_full_capacity;
  int8_t               m_soc;
  status               m_status;
  power();
};

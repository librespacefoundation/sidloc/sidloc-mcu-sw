/*
 *  SIDLOC MCU software: SIDLOC beacon software
 *
 *  Copyright (C) 2022, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "power.hpp"
#include "antenna.hpp"
#include <zephyr/drivers/i2c.h>
#include <zephyr/kernel.h>

static const struct device *i2c1 = DEVICE_DT_GET(DT_NODELABEL(i2c1));

extern "C" {

uint8_t
max17261_read_word(struct max17261_conf *conf, uint8_t reg, uint16_t *value)
{
  uint8_t buffer[2] = {0, 0};
  i2c_burst_read(i2c1, power::max17261_i2c_addr, reg, buffer, 2);
  *value = buffer[1];
  *value = (*value) << 8 | buffer[0];
  return 0;
}

uint8_t
max17261_write_word(struct max17261_conf *conf, uint8_t reg, uint16_t value)
{
  uint8_t buffer[2] = {static_cast<uint8_t>(value & 0xFF),
                       static_cast<uint8_t>(value >> 8)};
  i2c_burst_write(i2c1, power::max17261_i2c_addr, reg, buffer, 2);
  return 0;
}

uint8_t
max17261_delay_ms(struct max17261_conf *conf, uint32_t period)
{
  k_msleep(period);
  return 0;
}
}

void
power::update()
{
  m_voltage     = max17261_get_voltage(&m_max17261);
  m_voltage_avg = max17261_get_average_voltage(&m_max17261);
  max17261_get_minmax_voltage(&m_max17261, &m_voltage_min, &m_voltage_max);
  m_current     = max17261_get_current(&m_max17261);
  m_current_avg = max17261_get_average_current(&m_max17261);
  max17261_get_minmax_current(&m_max17261, &m_current_min, &m_current_max);
  m_temperature_die = max17261_get_die_temperature(&m_max17261);
  m_temperature     = max17261_get_temperature(&m_max17261);
  max17261_get_minmax_temperature(&m_max17261, &m_temperature_min,
                                  &m_temperature_max);
  m_tte                = max17261_get_TTE(&m_max17261);
  m_remaining_capacity = max17261_get_reported_capacity(&m_max17261);
  m_full_capacity      = max17261_get_full_reported_capacity(&m_max17261);
  m_soc                = max17261_get_SOC(&m_max17261);
  max17261_get_learned_params(&m_max17261);
  max17261_get_qrtable_values(&m_max17261);

  /* Update the operational status */
  if (m_voltage_min < critical_v_thrh) {
    m_status = status::CRITICAL;
    max17261_reset_minmax_voltage(&m_max17261);
    return;
  }

  switch (m_status) {
  case status::CRITICAL:
    if (m_voltage_avg > low_v_entry_thrh) {
      m_status = status::LOW;
      max17261_reset_minmax_voltage(&m_max17261);
    }
    break;
  case status::LOW:
    if (m_soc > low_soc_exit_thrh || m_voltage_avg > low_v_exit_thrh) {
      m_status = status::NORMAL;
      max17261_reset_minmax_voltage(&m_max17261);
    }
    break;
  case status::NORMAL:
    if (m_soc < low_soc_entry_thrh && m_voltage_min < low_v_entry_thrh) {
      m_status = status::LOW;
    }
  }
}

uint16_t
power::voltage() const
{
  return m_voltage;
}

uint16_t
power::voltage_avg() const
{
  return m_voltage_avg;
}

uint16_t
power::voltage_max() const
{
  return m_voltage_max;
}

uint16_t
power::voltage_min() const
{
  return m_voltage_min;
}

int16_t
power::current() const
{
  return m_current;
}

int16_t
power::current_avg() const
{
  return m_current_avg;
}

int16_t
power::current_max() const
{
  return m_current_max;
}

int16_t
power::current_min() const
{
  return m_current_min;
}

int8_t
power::temperature_die() const
{
  return m_temperature_die;
}

int8_t
power::temperature() const
{
  return m_temperature;
}

int8_t
power::temperature_avg() const
{
  return m_temperature_avg;
}

int8_t
power::temperature_max() const
{
  return m_temperature_max;
}

int8_t
power::temperature_min() const
{
  return m_temperature_min;
}

uint16_t
power::time_to_empty_mins() const
{
  return m_tte;
}

int16_t
power::remaining_capacity_mAh() const
{
  return m_remaining_capacity;
}

int16_t
power::full_capacity_mAh() const
{
  return m_full_capacity;
}

int8_t
power::charging_percentage() const
{
  return m_soc;
}

void
power::reset_minmax()
{
  max17261_reset_minmax_voltage(&m_max17261);
  max17261_reset_minmax_current(&m_max17261);
  max17261_reset_minmax_temperature(&m_max17261);
}

power::status
power::get_status() const
{
  return m_status;
}

power::power()
    : m_max17261{.DesignCap = battery_capacity_mAh,
                 .IchgTerm  = charge_term_i,
                 .VEmpty = (battery_v_empty << 7) | (battery_v_recovery & 0x7F),
                 .ChargeVoltage = charging_voltage,
                 .force_init    = 0,
                 .R100          = 1,
                 .init_option   = 2,
                 .lparams       = {.RCOMP0     = 0x368,
                                   .TempCo     = 0x8766,
                                   .FullCapRep = battery_capacity_mAh,
                                   .cycles     = 0,
                                   .FullCapNom = battery_capacity_mAh,
                                   .QRTable    = {0x1050, 0x2013, 0x5D86, 0x7FFF}}},
      m_voltage(0),
      m_voltage_avg(0),
      m_voltage_max(0),
      m_voltage_min(0),
      m_current(0),
      m_current_avg(0),
      m_current_max(0),
      m_current_min(0),
      m_temperature_die(0),
      m_temperature(0),
      m_temperature_avg(0),
      m_temperature_max(0),
      m_temperature_min(0),
      m_tte(0),
      m_remaining_capacity(0),
      m_full_capacity(0),
      m_soc(0),
      m_status(status::NORMAL)
{
  if (!device_is_ready(i2c1)) {
    k_oops();
  }
  if (max17261_init(&m_max17261) != 0) {
    k_oops();
  }

  /* If this is the first deploy reset all minmax estimations */
  if (antenna::get_instance().deployed() == false) {
    reset_minmax();
  }

  /* Make an update at contruction */
  update();
}
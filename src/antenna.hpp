/*
 *  SIDLOC MCU software: SIDLOC beacon software
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <cstdint>
#include <etl/debounce.h>

class antenna
{
public:
  static antenna &
  get_instance()
  {
    static antenna instance;
    return instance;
  }

  /* Singleton */
  antenna(antenna const &) = delete;

  void
  operator=(antenna const &) = delete;

  void
  deploy(int wdgid, bool bypass = false);

  bool
  deployed();

protected:
  antenna();

private:
  const uint32_t        m_dur_secs;
  const uint32_t        m_safe_margin_secs;
  etl::debounce<10, 10> m_dbounce;

  bool
  sense();
};

/*
 *  SIDLOC MCU software: SIDLOC beacon software
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "settings.hpp"
#include <cstddef>
#include <cstdint>
#include <etl/algorithm.h>
#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>
#include <zephyr/settings/settings.h>

LOG_MODULE_DECLARE(sidloc, CONFIG_LOG_DEFAULT_LEVEL);

struct direct_immediate_value
{
  size_t  len;
  void   *dest;
  uint8_t fetched;
};

static int
direct_loader_immediate_value(const char *name, size_t len,
                              settings_read_cb read_cb, void *cb_arg,
                              void *param)
{
  const char                    *next;
  size_t                         name_len;
  int                            rc;
  struct direct_immediate_value *one_value =
      (struct direct_immediate_value *)param;

  name_len = settings_name_next(name, &next);

  if (name_len == 0) {
    if (len == one_value->len) {
      rc = read_cb(cb_arg, one_value->dest, len);
      if (rc >= 0) {
        one_value->fetched = 1;
        return 0;
      }
      return rc;
    }
    return -EINVAL;
  }

  /* other keys aren't served by the callback
   * Return success in order to skip them
   * and keep storage processing.
   */
  return 0;
}

static int
load_immediate_value(const char *name, void *dest, size_t len)
{
  int                           rc;
  struct direct_immediate_value dov;

  dov.fetched = 0;
  dov.len     = len;
  dov.dest    = dest;

  rc = settings_load_subtree_direct(name, direct_loader_immediate_value,
                                    (void *)&dov);
  if (rc == 0) {
    if (!dov.fetched) {
      rc = -ENOENT;
    }
  }

  return rc;
}

bool
settings::antenna_deployed()
{
  /* We use full word granularrity for settings storage */
  uint32_t val = 0;
  int ret = load_immediate_value(setting_antenna_deployed, &val, sizeof(val));
  if (ret) {
    return default_ant_deployed;
  }
  return val == 1;
}

void
settings::set_antenna_deployed(bool success)
{
  uint32_t val = success & 0x1;
  settings_save_one(setting_antenna_deployed, &val, sizeof(val));
}

void
settings::set_tx_period(uint32_t ms)
{
  ms = etl::clamp<uint32_t>(ms, 12000, 60000);
  settings_save_one(setting_tx_period, &ms, sizeof(ms));
  LOG_INF("set TX period to %u ms", ms);
}

uint32_t
settings::tx_period()
{
  uint32_t val = 0;
  int      ret = load_immediate_value(setting_tx_period, &val, sizeof(val));
  if (ret) {
    return default_tx_period_ms;
  }
  return val;
}

void
settings::set_rx_period(uint32_t ms)
{
  ms = etl::clamp<uint32_t>(ms, 4000, 60000);
  settings_save_one(setting_rx_period, &ms, sizeof(ms));
  LOG_INF("set RX period to %u ms", ms);
}

uint32_t
settings::rx_period()
{
  uint32_t val = 0;
  int      ret = load_immediate_value(setting_rx_period, &val, sizeof(val));
  if (ret) {
    return default_rx_period_ms;
  }
  return val;
}

void
settings::set_idle_period(uint32_t ms)
{
  ms = etl::clamp<uint32_t>(ms, 1000, 3 * 60000);
  settings_save_one(setting_idle_period, &ms, sizeof(ms));
  LOG_INF("set IDLE period to %u ms", ms);
}

uint32_t
settings::idle_period()
{
  uint32_t val = 0;
  int      ret = load_immediate_value(setting_idle_period, &val, sizeof(val));
  if (ret) {
    return default_idle_period_ms;
  }
  return val;
}

bool
settings::tx_inhibit()
{
  uint32_t val = 0;
  int      ret = load_immediate_value(setting_tx_inhibit, &val, sizeof(val));
  if (ret) {
    return default_tx_inhibit;
  }

  /* TX inhibit is a dangerous setting, so we allow some bits to be off */
  std::bitset<32> x(val);
  x ^= tx_inhibit_magic_val;
  return x.count() < 6;
}

void
settings::set_tx_inhibit(bool yes)
{
  uint32_t val = 0;
  if (yes) {
    val = tx_inhibit_magic_val.to_ulong();
  }
  settings_save_one(setting_tx_inhibit, &val, sizeof(val));
  LOG_INF("set TX inhibit to %u", val);
}

settings::settings()
{
  /*
   * NOTE: This is Ariane 6 specific and might change in the future. Even if the
   * settings subsystem fails to initialize, we can recover. The side effect is
   * the deployement sequence will be repeated each time a reboot occurs
   */
  settings_subsys_init();
}

/*
 *  SIDLOC MCU software: SIDLOC beacon software
 *
 *  Copyright (C) 2022, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <at86rf215.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/kernel.h>

/*
 * This is a weak function of the AT86RF215 driver allowing user defined
 * processing of the IRQs. We need to forward declare it here in order
 * to define it as friend of the radio class.
 * With this trick, this callback can access the private fields of the radio
 * class. This eliminate the need for the radio class to expose at the public
 * API, dangerous/unnecessary methods
 */
extern "C" int
at86rf215_irq_user_callback(struct at86rf215 *h, uint8_t rf09_irqs,
                            uint8_t rf24_irqs, uint8_t bbc0_irqs,
                            uint8_t bbc1_irqs);

class radio
{
public:
  static constexpr uint32_t rx_pdu_len  = CONFIG_SIDLOC_RX_FRAME_LEN;
  static constexpr uint32_t spi_max_len = 2048;

  enum class op_mode : uint8_t
  {
    BASEBAND = 0,
    IQ
  };

  enum class direction : uint8_t
  {
    RX = 0,
    TX
  };

  static radio &
  get_instance()
  {
    static radio instance;
    return instance;
  }

  /* Singleton */
  radio(radio const &) = delete;

  void
  operator=(radio const &) = delete;

  int
  enable(bool yes = true);

  bool
  enabled() const;

  int
  tranmsit_bb(const uint8_t *msg, uint32_t length);

  int
  iq_start(direction dir, at86rf215_sr_t sr);

  int
  set_frequency(float freq);

  int
  iq_stop(direction dir);

  void
  set_gain(float val);

  void
  rx_async();

private:
  /* IQIFSF TRXERR BATLOW EDC TRXRDY*/
  static constexpr uint8_t TX_RF_IRQM = 0xFE;
  /* All IRQs for the baseband core enabled during TX */
  static constexpr uint8_t TX_BB_IRQM = 0xFF;

  /* IQIFSF TRXERR BATLOW EDC TRXRDY*/
  static constexpr uint8_t RX_RF_IRQM = 0xFE;

  /* RXFE, RXFS */
  static constexpr uint8_t RX_BB_IRQM = 0x3;

  bool                 m_enabled;
  struct at86rf215     m_at86;
  struct gpio_callback m_trx_irq_clbk;
  struct k_work_q      m_work_q;
  struct k_work        m_irq_work;
  uint8_t              m_rx_buf[AT86RF215_MAX_PDU];
  radio();

  int
  set_iq_mode(direction dir, at86rf215_sr_t sr);

  int
  set_bb_mode(direction dir);

  static void
  trx_irq_callback(const struct device *dev, struct gpio_callback *cb,
                   uint32_t pins);

  static void
  irq_handler(struct k_work *item);

  friend int
  at86rf215_irq_user_callback(struct at86rf215 *h, uint8_t rf09_irqs,
                              uint8_t rf24_irqs, uint8_t bbc0_irqs,
                              uint8_t bbc1_irqs);
};

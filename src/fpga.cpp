/*
 *  SIDLOC MCU software: SIDLOC beacon software
 *
 *  Copyright (C) 2022, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "fpga.hpp"
#include <algorithm>
#include <zephyr/drivers/gpio.h>
#include <zephyr/drivers/spi.h>

static const struct gpio_dt_spec fpga_en =
    GPIO_DT_SPEC_GET_BY_IDX(DT_NODELABEL(fpga_en), gpios, 0);

/*
 * Note: SPI clock should be at least /4 the clock speed that
 * the SPI IP core is running (currently 26 MHz)
 */
static const struct spi_config spi_cfg = {
    .frequency = 1000000,
    .operation = SPI_OP_MODE_MASTER | SPI_TRANSFER_MSB | SPI_WORD_SET(8) |
                 SPI_LINES_SINGLE | SPI_FULL_DUPLEX | SPI_FRAME_FORMAT_MOTOROLA,
    .cs = {.gpio  = SPI_CS_GPIOS_DT_SPEC_GET(DT_NODELABEL(fpga_spi)),
           .delay = 100}};

static uint8_t                  rx_msg[fpga::spi_max_len];
static struct spi_buf           rx;
const static struct spi_buf_set rx_bufs = {
    .buffers = &rx,
    .count   = 1,
};

static uint8_t        tx_msg[fpga::spi_max_len];
static struct spi_buf tx = {.buf = tx_msg, .len = fpga::spi_max_len};

const static struct spi_buf_set tx_bufs = {
    .buffers = &tx,
    .count   = 1,
};

static const struct device *fpga_spi_dev = DEVICE_DT_GET(DT_NODELABEL(spi1));

/**
 * @brief Enables/disables the power rail of the FPGA. Enabling the power rail
 * it does not mean that the FPGA is up and running. Use the \p check() for
 * this.
 *
 * @param yes true to enable, false to disable
 */
void
fpga::enable(bool yes)
{
  if (yes) {
    gpio_pin_set_dt(&fpga_en, 1);
    m_enabled = true;
    k_msleep(2000);
  } else {
    disable();
  }
}

void
fpga::led(bool en)
{
  write(reg::LED, en);
}

/**
 * @brief Disables the power rail of the FPGA
 *
 */
void
fpga::disable()
{
  gpio_pin_set_dt(&fpga_en, 0);
  m_enabled = false;
}

/**
 * @brief Check if the FPGA is enabled and operational
 * @return true if the FPGA is enabled and operational, false otherwise
 */
bool
fpga::check()
{
  if (!m_enabled) {
    return false;
  }

  uint8_t val;
  int     ret = read(reg::ID, &val);
  if (ret || val != id) {
    return false;
  }

  ret = write(reg::SCRATCH, 0x0);
  if (ret) {
    return false;
  }

  ret = read(reg::SCRATCH, &val);
  if (ret) {
    return false;
  }

  ret = write(reg::SCRATCH, scratch);
  if (ret) {
    return false;
  }

  ret = read(reg::SCRATCH, &val);
  if (ret) {
    return false;
  }
  return val == scratch;
}

int
fpga::read(reg r, uint8_t *out)
{
  switch (r) {
  case reg::ID:
  case reg::SCRATCH:
  case reg::LED:
  case reg::GPIO:
  case reg::IQ_RX:
  case reg::IQ_TX:
    return read(static_cast<uint8_t>(r), out);
  default:
    return -EINVAL;
  }
}

int
fpga::read(uint8_t reg, uint8_t *out)
{
  reg &= 0x7F;
  auto b = reinterpret_cast<uint8_t *>(tx.buf);
  b[0]   = reg;

  uint8_t miso[2] = {0, 0};

  /* Lame API */
  tx.len  = 1;
  rx.buf  = miso;
  rx.len  = 2;
  int ret = spi_transceive(fpga_spi_dev, &spi_cfg, &tx_bufs, &rx_bufs);
  if (ret) {
    return ret;
  }
  *out = miso[1];
  return 0;
}

int
fpga::write(reg r, uint8_t value)
{
  switch (r) {
  case reg::ID:
  case reg::SCRATCH:
  case reg::LED:
  case reg::GPIO:
  case reg::IQ_RX:
  case reg::IQ_TX:
    return write(static_cast<uint8_t>(r), value);
  default:
    return -EINVAL;
  }
}

int
fpga::write(uint8_t reg, uint8_t value)
{
  uint8_t tmp[2];
  reg |= BIT(7);
  auto b = reinterpret_cast<uint8_t *>(tx.buf);
  b[0]   = reg;
  b[1]   = value;

  /* Lame API */
  tx.len = 2;
  rx.buf = tmp;
  rx.len = 2;
  return spi_transceive(fpga_spi_dev, &spi_cfg, &tx_bufs, &rx_bufs);
}

fpga::fpga() : m_enabled(false)
{
  gpio_pin_configure_dt(&fpga_en, GPIO_OUTPUT_ACTIVE | fpga_en.dt_flags);
  gpio_pin_set_dt(&fpga_en, 0);
}

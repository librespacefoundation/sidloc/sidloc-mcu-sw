/*
 *  SIDLOC MCU software: SIDLOC beacon software
 *
 *  Copyright (C) 2022-2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "radio.hpp"
#include "ccsds.hpp"
#include "fpga.hpp"
#include "settings.hpp"
#include <algorithm>
#include <cmath>
#include <etl/algorithm.h>
#include <etl/binary.h>
#include <etl/crc.h>
#include <pb_decode.h>
#include <proto/telecommands.pb.h>
#include <tinycrypt/sha256.h>
#include <zephyr/drivers/spi.h>
#include <zephyr/sys/reboot.h>

/* The C++ way. This is the way! */
template <typename T, const size_t IDX,
          std::enable_if_t<std::is_integral_v<T>, bool> = true>
static constexpr T
bit()
{
  static_assert(IDX < sizeof(T) * 8, "The operation will overflow!");
  return static_cast<T>(1) << IDX;
}

static constexpr std::array<uint8_t, 32>
sha256_key_hex2bytes()
{
  std::array<uint8_t, 32> key = {0};
  static_assert(std::char_traits<char>::length(CONFIG_TLC_SHA256_KEY) == 64,
                "Hash should be exactly 64 hex annotated chars");
  for (unsigned int i = 0; i < 64; i += 2) {
    char s[2]  = {CONFIG_TLC_SHA256_KEY[i], CONFIG_TLC_SHA256_KEY[i + 1]};
    char byte  = (char)strtol(s, NULL, 16);
    key[i / 2] = byte;
  }
  return key;
}

static const auto                   &tlc_key = sha256_key_hex2bytes();
static struct tc_sha256_state_struct sha256_state;
static std::array<uint8_t, 32>       sha256_comp;

static const struct gpio_dt_spec trx_en =
    GPIO_DT_SPEC_GET_BY_IDX(DT_NODELABEL(trx_en), gpios, 0);
static const struct gpio_dt_spec trx_rst =
    GPIO_DT_SPEC_GET_BY_IDX(DT_NODELABEL(trx_rst), gpios, 0);
static const struct gpio_dt_spec trx_irq =
    GPIO_DT_SPEC_GET_BY_IDX(DT_NODELABEL(trx_irq), gpios, 0);
static const struct gpio_dt_spec rffe_en =
    GPIO_DT_SPEC_GET_BY_IDX(DT_NODELABEL(rffe_en), gpios, 0);
static const struct gpio_dt_spec rffe_tx_en =
    GPIO_DT_SPEC_GET_BY_IDX(DT_NODELABEL(rffe_tx_en), gpios, 0);

static const struct spi_config spi_cfg = {
    .frequency = 5000000,
    .operation = SPI_OP_MODE_MASTER | SPI_TRANSFER_MSB | SPI_WORD_SET(8) |
                 SPI_LINES_SINGLE | SPI_FULL_DUPLEX | SPI_FRAME_FORMAT_MOTOROLA,
    .cs = {.gpio  = SPI_CS_GPIOS_DT_SPEC_GET(DT_NODELABEL(at86_spi)),
           .delay = 200},
};

static const struct device *at86_spi_dev = DEVICE_DT_GET(DT_NODELABEL(spi1));

static uint8_t                  rx_msg[radio::spi_max_len];
static struct spi_buf           rx = {.buf = rx_msg, .len = radio::spi_max_len};
const static struct spi_buf_set rx_bufs = {
    .buffers = &rx,
    .count   = 1,
};

static uint8_t        tx_msg[radio::spi_max_len];
static struct spi_buf tx = {.buf = tx_msg, .len = radio::spi_max_len};

const static struct spi_buf_set tx_bufs = {
    .buffers = &tx,
    .count   = 1,
};

K_THREAD_STACK_DEFINE(irq_workqueue_thread_stack, 2048);

static uplink_tlc tlc;

void
radio::trx_irq_callback(const struct device *dev, struct gpio_callback *cb,
                        uint32_t pins)
{
  auto &r = radio::get_instance();
  k_work_submit_to_queue(&r.m_work_q, &r.m_irq_work);
}

/* Implement the platform specific stuff for the AT86RF215 driver */
extern "C" {
void
at86rf215_delay_us(struct at86rf215 *h, uint32_t us)
{
  k_usleep(us);
}

size_t
at86rf215_get_time_ms(struct at86rf215 *h)
{
  return k_uptime_get();
}

int
at86rf215_set_rstn(struct at86rf215 *h, uint8_t enable)
{
  gpio_pin_set_dt(&trx_rst, !enable & 0x1);
  return AT86RF215_OK;
}

/**
 * Reads from the SPI peripheral
 * @param out the output buffer to hold MISO response from the SPI peripheral
 * @param in input buffer containing MOSI data
 * @param tx_len the length of the MOSI SPI transaction
 * @param rx_len the length of the MISO SPI transaction
 * @return 0 on success or negative error code
 */
int
at86rf215_spi_read(struct at86rf215 *h, uint8_t *out, const uint8_t *in,
                   size_t tx_len, size_t rx_len)
{
  if (tx_len > radio::spi_max_len || rx_len > radio::spi_max_len) {
    return -EINVAL;
  }
  /* Lame API */
  std::copy_n(in, tx_len, reinterpret_cast<uint8_t *>(tx.buf));
  tx.len = tx_len;
  rx.buf = out;
  rx.len = rx_len;
  return spi_transceive(at86_spi_dev, &spi_cfg, &tx_bufs, &rx_bufs);
}

/**
 * Writes to the device using the SPI peripheral
 * @param in the input buffer
 * @param len the size of the input buffer
 * @return 0 on success or negative error code
 */
int
at86rf215_spi_write(struct at86rf215 *h, const uint8_t *in, size_t len)
{
  if (len > radio::spi_max_len) {
    return -EINVAL;
  }
  /* Lame API */
  std::copy_n(in, len, reinterpret_cast<uint8_t *>(tx.buf));
  tx.len = len;
  return spi_write(at86_spi_dev, &spi_cfg, &tx_bufs);
}

/**
 * Enable/disable the AT86RF215 IRQ line
 * @note Especially for bare metal applications (no-OS), race conditions may
 * occur, while a write SPI transaction activates an IRQ. The IRQ handler will
 * initiate an SPI read transaction before the end of the SPI write. Users may
 * override the default implementation of this function according to their
 * execution environment.
 *
 * @param enable 1 to enable, 0 to disable
 * @return 0 on success or negative error code
 */
int
at86rf215_irq_enable(struct at86rf215 *h, uint8_t enable)
{
  /*
   * In Zephyr the SPI transactions necessary after an IRQ event, are handled
   * inside a workqueue and not in the ISR. Therefore there is no need for
   * enabling/disabling the IRQ
   */
  return 0;
}

int
at86rf215_irq_user_callback(struct at86rf215 *h, uint8_t rf09_irqs,
                            uint8_t rf24_irqs, uint8_t bbc0_irqs,
                            uint8_t bbc1_irqs)
{
  radio *r = reinterpret_cast<radio *>(h->user_dev1);
  /* Check if a frame has been received */
  if (bbc0_irqs & bit<uint8_t, 1>()) {
    // FIXME USE DMA!
    uint32_t pdu_len = r->rx_pdu_len;
    at86rf215_rx_frame(h, AT86RF215_RF09, r->m_rx_buf, pdu_len);
    /* After the RXFE the AT86 does not enter RX mode */
    at86rf215_set_cmd(h, AT86RF215_CMD_RF_RX, AT86RF215_RF09);

    /* Check the CRC */
    auto crc =
        etl::crc32_c(r->m_rx_buf, r->m_rx_buf + pdu_len - sizeof(uint32_t))
            .value();
    uint32_t recv = (r->m_rx_buf[pdu_len - sizeof(uint32_t)] << 24) |
                    (r->m_rx_buf[pdu_len - sizeof(uint32_t) + 1] << 16) |
                    (r->m_rx_buf[pdu_len - sizeof(uint32_t) + 2] << 8) |
                    r->m_rx_buf[pdu_len - sizeof(uint32_t) + 3];
    if (crc != recv) {
      return AT86RF215_OK;
    }
    pdu_len -= 4;

    pb_istream_t stream = pb_istream_from_buffer(r->m_rx_buf, pdu_len);
    bool         ok     = pb_decode_delimited(&stream, uplink_tlc_fields, &tlc);
    /* Nothing more to do */
    if (!ok) {
      return AT86RF215_OK;
    }

    if (tlc.spcid != CONFIG_SPACECRAFT_ID) {
      return AT86RF215_OK;
    }

    /* Check the key */
    tc_sha256_init(&sha256_state);
    tc_sha256_update(&sha256_state, tlc.key.bytes, tlc.key.size);
    tc_sha256_final(sha256_comp.data(), &sha256_state);
    if (sha256_comp != tlc_key) {
      return AT86RF215_OK;
    }

    auto &cnf = settings::get_instance();
    switch (tlc.tlc.which_telecommand) {
    case telecommands_periods_tag:
      cnf.set_tx_period(tlc.tlc.telecommand.periods.tx_ms);
      cnf.set_rx_period(tlc.tlc.telecommand.periods.rx_ms);
      cnf.set_idle_period(tlc.tlc.telecommand.periods.idle_ms);
      break;
    case telecommands_tx_inhibit_tag:
      cnf.set_tx_inhibit(tlc.tlc.telecommand.tx_inhibit.enable);
      break;
    case telecommands_ant_deploy_tag:
      /*
       * Reset the deploy state and make a reboot. This eventually will force to
       * redo the antenna deploy, simplifying the logic required
       */
      cnf.set_antenna_deployed(false);
      sys_reboot(SYS_REBOOT_COLD);
      break;
    default:
      return AT86RF215_OK;
    }
  }
  return AT86RF215_OK;
}
}

int
radio::enable(bool yes)
{
  if (!yes) {
    /* Disable the power */
    gpio_pin_set_dt(&rffe_tx_en, 0);
    gpio_pin_set_dt(&rffe_en, 0);
    gpio_pin_set_dt(&trx_en, 0);
    gpio_pin_interrupt_configure_dt(&trx_irq, GPIO_INT_DISABLE);
    m_enabled = false;
    return SUCCESS;
  }

  m_at86.clk_drv      = AT86RF215_RF_DRVCLKO8;
  m_at86.clko_os      = AT86RF215_RF_CLKO_26_MHZ;
  m_at86.xo_trim      = 0;
  m_at86.xo_fs        = 0;
  m_at86.irqp         = 0;
  m_at86.irqmm        = 1;
  m_at86.pad_drv      = AT86RF215_RF_DRV8;
  m_at86.rf_femode_09 = AT86RF215_RF_FEMODE2;
  m_at86.rf_femode_24 = AT86RF215_RF_FEMODE2;

  /* Enable the power */
  gpio_pin_set_dt(&trx_en, 1);
  k_msleep(10);
  int ret = at86rf215_init(&m_at86);
  if (ret) {
    return ret;
  }

  ret = at86rf215_set_cmd(&m_at86, AT86RF215_CMD_RF_TRXOFF, AT86RF215_RF09);
  if (ret) {
    return ret;
  }

  const struct at86rf215_radio_conf rconf = {.cm  = AT86RF215_CM_FINE_RES_04,
                                             .lbw = AT86RF215_PLL_LBW_SMALLER};
  ret = at86rf215_radio_conf(&m_at86, AT86RF215_RF09, &rconf);
  if (ret) {
    return ret;
  }

  /* Disable completely the 2.4 interface IRQs */
  ret = at86rf215_set_bbc_irq_mask(&m_at86, AT86RF215_RF24, 0x00);
  if (ret) {
    return ret;
  }
  ret = at86rf215_set_radio_irq_mask(&m_at86, AT86RF215_RF24, 0x0);
  if (ret) {
    return ret;
  }

  ret = at86rf215_set_radio_irq_mask(&m_at86, AT86RF215_RF09, 0xFE);
  if (ret) {
    return ret;
  }

  ret = at86rf215_set_bbc_irq_mask(&m_at86, AT86RF215_RF09, 0xFF);
  if (ret) {
    return ret;
  }

  ret = at86rf215_set_freq(&m_at86, AT86RF215_RF09, 401.5e6);
  if (ret) {
    return ret;
  }

  ret = at86rf215_set_pac(&m_at86, AT86RF215_RF09, AT86RF215_PACUR_NO_RED, 30);
  if (ret) {
    return ret;
  }

  /* Before enabling the IRQs make sure that are clear */
  at86rf215_irq_clear(&m_at86);

  gpio_pin_interrupt_configure_dt(&trx_irq, GPIO_INT_EDGE_TO_ACTIVE);

  at86rf215_pll_ls_t pll_ls;
  ret = at86rf215_get_pll_ls(&m_at86, &pll_ls, AT86RF215_RF09);
  if (ret) {
    return ret;
  }

  at86rf215_rf_state_t s;
  ret = at86rf215_get_state(&m_at86, &s, AT86RF215_RF09);
  if (ret) {
    return ret;
  }

  ret = at86rf215_bb_enable(&m_at86, AT86RF215_RF09, 1);
  if (ret) {
    return ret;
  }

  /* Enable the RFFE */
  gpio_pin_set_dt(&rffe_en, 1);

  m_enabled = true;
  return SUCCESS;
}

bool
radio::enabled() const
{
  return m_enabled;
}

int
radio::tranmsit_bb(const uint8_t *msg, uint32_t length)
{
  return at86rf215_tx_frame(&m_at86, AT86RF215_RF09, msg, length, 4000);
}

int
radio::iq_start(direction dir, at86rf215_sr_t sr)
{
  auto &f = fpga::get_instance();

  int ret = set_iq_mode(dir, sr);
  if (ret) {
    return ret;
  }

  if (dir == direction::TX) {
    /* Enable the RFFE TX path */
    gpio_pin_set_dt(&rffe_tx_en, 1);

    uint8_t val = 0;
    /*
     * Apply SYNC for TX amd the given sampling rate. Also reset the AT86RF215
     * IQ TX core, as new sampling rate is applied only under reset
     */
    val = BIT(5) | (static_cast<uint8_t>(sr) << 1);
    ret = f.write(fpga::reg::IQ_TX, val);
    if (ret) {
      return ret;
    }
    k_msleep(10);
    val |= 1;
    /* Enable it! */
    ret = f.write(fpga::reg::IQ_TX, val);
  } else {
    gpio_pin_set_dt(&rffe_tx_en, 0);
    uint8_t val = 0;
    /*
     * Apply SYNC for TX amd the given sampling rate. Also reset the AT86RF215
     * IQ TX core, as new sampling rate is applied only under reset
     */
    val = (static_cast<uint8_t>(sr) << 1);
    ret = f.write(fpga::reg::IQ_RX, val);
    if (ret) {
      return ret;
    }
    k_msleep(10);
    val |= 1;
    /* Enable it! */
    ret = f.write(fpga::reg::IQ_RX, val);
  }
  return ret;
}

int
radio::set_frequency(float freq)
{
  return at86rf215_set_freq(&m_at86, AT86RF215_RF09, freq);
}

int
radio::iq_stop(direction dir)
{
  int   ret = 0;
  auto &f   = fpga::get_instance();
  gpio_pin_set_dt(&rffe_tx_en, 0);
  if (dir == direction::TX) {
    uint8_t val = 0;
    ret         = f.read(fpga::reg::IQ_TX, &val);
    if (ret) {
      return ret;
    }
    val &= 0b11110;
    ret = f.write(fpga::reg::IQ_TX, val);
  } else {
    uint8_t val = 0;
    ret         = f.read(fpga::reg::IQ_RX, &val);
    if (ret) {
      return ret;
    }
    val &= 0b111110;
    ret = f.write(fpga::reg::IQ_RX, val);
  }
  return ret;
}

void
radio::set_gain(float val)
{
  val = etl::clamp(val, 0.0f, 31.0f);
  at86rf215_set_pac(&m_at86, AT86RF215_RF09, AT86RF215_PACUR_NO_RED,
                    std::round(val));
}

void
radio::rx_async()
{
  gpio_pin_set_dt(&rffe_tx_en, 0);
  int ret = at86rf215_set_radio_irq_mask(&m_at86, AT86RF215_RF09, RX_RF_IRQM);
  ret     = at86rf215_set_bbc_irq_mask(&m_at86, AT86RF215_RF09, RX_BB_IRQM);

  /* Set the AGC target */
  at86rf215_agc_conf agc_cnf = {.enable = 1,
                                .freeze = 0,
                                .reset  = 0,
                                .avgs   = AT86RF215_AVGS_8,
                                .input  = 0,
                                .gcw    = 23,
                                .tgt    = AT86RF215_TGT_M21};
  at86rf215_set_agc(&m_at86, AT86RF215_RF09, &agc_cnf);

  /* FSK-2 50kHz */
  struct at86rf215_bb_conf at86_bb;
  at86_bb.ctx                    = 0;
  at86_bb.fcsfe                  = 0;
  at86_bb.txafcs                 = 1;
  at86_bb.fcst                   = AT86RF215_FCS_16;
  at86_bb.pt                     = AT86RF215_BB_MRFSK;
  at86_bb.fsk.mord               = AT86RF215_2FSK;
  at86_bb.fsk.midx               = AT86RF215_MIDX_3;
  at86_bb.fsk.midxs              = AT86RF215_MIDXS_88;
  at86_bb.fsk.bt                 = AT86RF215_FSK_BT_10;
  at86_bb.fsk.srate              = AT86RF215_FSK_SRATE_50;
  at86_bb.fsk.fi                 = 0;
  at86_bb.fsk.rxo                = AT86RF215_FSK_RXO_DISABLED;
  at86_bb.fsk.pdtm               = 0;
  at86_bb.fsk.rxpto              = 1;
  at86_bb.fsk.mse                = 0;
  at86_bb.fsk.pri                = 0;
  at86_bb.fsk.fecs               = AT86RF215_FSK_FEC_RSC;
  at86_bb.fsk.fecie              = 0;
  at86_bb.fsk.sfd_threshold      = 8;
  at86_bb.fsk.preamble_threshold = 8;
  at86_bb.fsk.sfdq               = 0;
  at86_bb.fsk.sfd32              = 1;
  at86_bb.fsk.rawrbit            = 1; // Most of the encoders transmit MSB first
  at86_bb.fsk.csfd1              = AT86RF215_SFD_UNCODED_RAW;
  at86_bb.fsk.csfd0              = AT86RF215_SFD_UNCODED_RAW;
  at86_bb.fsk.preamble_length    = 8;
  /* SFD operates in a LSB order*/
  at86_bb.fsk.sfd0 = static_cast<uint16_t>(etl::reverse_bits(ccsds::ASM));
  at86_bb.fsk.sfd1 = static_cast<uint16_t>(etl::reverse_bits(ccsds::ASM) >> 16);
  at86_bb.fsk.sfd  = 0;
  at86_bb.fsk.dw   = 0;
  at86_bb.fsk.preemphasis = 0;
  at86_bb.fsk.dm          = 1;

  ret = at86rf215_bb_conf(&m_at86, AT86RF215_RF09, &at86_bb);
  ret = at86rf215_set_mode(&m_at86, AT86RF215_RF_MODE_BBRF);
  ret = at86rf215_irq_clear(&m_at86);
  ret = at86rf215_bb_enable(&m_at86, AT86RF215_RF09, 1);
  ret = at86rf215_rx(&m_at86, AT86RF215_RF09, 1000);
}

radio::radio() : m_enabled(false)
{
  /*
   * user1 has the class itself for handling the user defined IRQ handler of
   * the AT86RF215 driver
   */
  m_at86.user_dev1 = this;

  gpio_pin_configure_dt(&trx_en, GPIO_OUTPUT_INACTIVE | trx_en.dt_flags);
  gpio_pin_configure_dt(&trx_rst, GPIO_OUTPUT_INACTIVE | trx_rst.dt_flags);
  gpio_pin_configure_dt(&rffe_en, GPIO_OUTPUT_INACTIVE | rffe_en.dt_flags);
  gpio_pin_configure_dt(&rffe_tx_en,
                        GPIO_OUTPUT_INACTIVE | rffe_tx_en.dt_flags);
  gpio_pin_configure_dt(&trx_irq, GPIO_INPUT | trx_irq.dt_flags);

  gpio_init_callback(&m_trx_irq_clbk, trx_irq_callback, BIT(trx_irq.pin));
  gpio_add_callback(trx_irq.port, &m_trx_irq_clbk);

  if (!device_is_ready(at86_spi_dev)) {
    // FIXME maybe report something?
    k_oops();
  }

  k_work_queue_start(&m_work_q, irq_workqueue_thread_stack,
                     K_THREAD_STACK_SIZEOF(irq_workqueue_thread_stack), 4,
                     NULL);
  k_work_init(&m_irq_work, &radio::irq_handler);
  gpio_pin_interrupt_configure_dt(&trx_irq, GPIO_INT_EDGE_TO_ACTIVE);
}

int
radio::set_iq_mode(direction dir, at86rf215_sr_t sr)
{
  const struct at86rf215_iq_conf cnf = {.extlb  = 0,
                                        .drv    = AT86RF215_LVDS_DRV4,
                                        .cmv1v2 = 1,
                                        .eec    = 1,
                                        .skedrv = 0x2,
                                        .tsr    = sr,
                                        .trcut  = 0x4,
                                        .rsr    = sr,
                                        .rrcut  = 0x4};
  int ret = at86rf215_iq_conf(&m_at86, AT86RF215_RF09, &cnf);
  if (ret) {
    return ret;
  }

  ret = at86rf215_set_mode(&m_at86, AT86RF215_RF_MODE_BBRF09);
  if (ret) {
    return ret;
  }

  ret = at86rf215_set_cmd(&m_at86, AT86RF215_CMD_RF_TXPREP, AT86RF215_RF09);
  if (ret) {
    return ret;
  }

  if (dir == direction::RX) {
    ret = at86rf215_set_cmd(&m_at86, AT86RF215_CMD_RF_RX, AT86RF215_RF09);
  } else {
    ret = at86rf215_set_cmd(&m_at86, AT86RF215_CMD_RF_TX, AT86RF215_RF09);
  }
  if (ret) {
    return ret;
  }

  at86rf215_rf_state_t s;
  ret = at86rf215_get_state(&m_at86, &s, AT86RF215_RF09);
  if (ret) {
    return ret;
  }

  at86rf215_pll_ls_t pll_ls;
  ret = at86rf215_get_pll_ls(&m_at86, &pll_ls, AT86RF215_RF09);
  if (ret) {
    return ret;
  }

  return AT86RF215_OK;
}

int
radio::set_bb_mode(direction dir)
{
  struct at86rf215_bb_conf bb;
  bb.ctx                    = 0;
  bb.fcsfe                  = 0;
  bb.txafcs                 = 1;
  bb.fcst                   = AT86RF215_FCS_16;
  bb.pt                     = AT86RF215_BB_MRFSK;
  bb.fsk.mord               = AT86RF215_2FSK;
  bb.fsk.midx               = AT86RF215_MIDX_3;
  bb.fsk.midxs              = AT86RF215_MIDXS_88;
  bb.fsk.bt                 = AT86RF215_FSK_BT_10;
  bb.fsk.srate              = AT86RF215_FSK_SRATE_50;
  bb.fsk.fi                 = 0;
  bb.fsk.preamble_length    = 256;
  bb.fsk.rxo                = AT86RF215_FSK_RXO_DISABLED;
  bb.fsk.pdtm               = 0;
  bb.fsk.rxpto              = 1;
  bb.fsk.mse                = 0;
  bb.fsk.pri                = 0;
  bb.fsk.fecs               = AT86RF215_FSK_FEC_RSC;
  bb.fsk.fecie              = 0;
  bb.fsk.sfd_threshold      = 15;
  bb.fsk.preamble_threshold = 8;
  bb.fsk.sfdq               = 1;
  bb.fsk.sfd32              = 1;
  bb.fsk.rawrbit            = 0;
  bb.fsk.csfd1              = AT86RF215_SFD_UNCODED_IEEE;
  bb.fsk.csfd0              = AT86RF215_SFD_UNCODED_IEEE;
  bb.fsk.preamble_length    = 64;
  bb.fsk.sfd                = 0;
  bb.fsk.dw                 = 0;
  bb.fsk.preemphasis        = 0;
  bb.fsk.dm                 = 1;

  int ret = at86rf215_bb_conf(&m_at86, AT86RF215_RF09, &bb);
  if (ret) {
    return ret;
  }

  ret = at86rf215_set_mode(&m_at86, AT86RF215_RF_MODE_BBRF);
  if (ret) {
    return ret;
  }

  ret = at86rf215_set_cmd(&m_at86, AT86RF215_CMD_RF_TXPREP, AT86RF215_RF09);
  if (ret) {
    return ret;
  }

  ret = at86rf215_bb_enable(&m_at86, AT86RF215_RF09, true);
  if (ret) {
    return ret;
  }

  return AT86RF215_OK;
}

void
radio::irq_handler(struct k_work *item)
{
  auto &r = radio::get_instance();
  at86rf215_irq_callback(&r.m_at86);
}

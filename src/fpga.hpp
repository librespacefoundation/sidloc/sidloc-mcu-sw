/*
 *  SIDLOC MCU software: SIDLOC beacon software
 *
 *  Copyright (C) 2022, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <cstdint>

class fpga
{
public:
  static constexpr uint32_t spi_max_len = 256;

  static constexpr uint8_t id      = 0x37;
  static constexpr uint8_t scratch = 0x4B;

  enum class reg : uint8_t
  {
    ID      = 0,
    SCRATCH = 1,
    LED     = 2,
    GPIO    = 3,
    IQ_RX   = 4,
    IQ_TX   = 5
  };

  static fpga &
  get_instance()
  {
    static fpga instance;
    return instance;
  }

  /* Singleton */
  fpga(fpga const &) = delete;

  void
  operator=(fpga const &) = delete;

  void
  enable(bool yes = true);

  void
  led(bool en);

  void
  disable();

  bool
  check();

  int
  read(reg r, uint8_t *out);

  int
  read(uint8_t reg, uint8_t *out);

  int
  write(reg r, uint8_t value);

  int
  write(uint8_t reg, uint8_t value);

private:
  bool m_enabled;

  fpga();
};
/*
 *  SIDLOC MCU software: SIDLOC beacon software
 *
 *  Copyright (C) 2022-2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */
#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>
/* Register the log module */
LOG_MODULE_REGISTER(sidloc);

#include "antenna.hpp"
#include "fpga.hpp"
#include "power.hpp"
#include "radio.hpp"
#include "settings.hpp"
#include "version.h"
#include <errno.h>
#include <zephyr/devicetree.h>
#include <zephyr/stats/stats.h>
#include <zephyr/sys/reboot.h>
#include <zephyr/sys/util.h>
#include <zephyr/task_wdt/task_wdt.h>

#if DT_HAS_COMPAT_STATUS_OKAY(st_stm32_watchdog)
#define WDT_NODE DT_COMPAT_GET_ANY_STATUS_OKAY(st_stm32_watchdog)
#else
#error "IWDG is not available. Check the device tree configuration"
#endif

K_THREAD_STACK_DEFINE(workqueue_thread_stack, 1024);
struct k_work_q main_work_q;
static k_work   pwr_update;
static k_timer  pwr_update_timer;

static void
pwr_update_timer_handler(struct k_timer *t);

static void
pwr_update_worker(struct k_work *item);

/* Task watchdog callback */
static void
task_wdt_callback(int channel_id, void *user_data)
{
  printk("Task watchdog channel %d callback", channel_id);
  /*
   *
   * If the issue could be resolved, call task_wdt_feed(channel_id) here
   * to continue operation.
   *
   * Otherwise we can perform some cleanup and reset the device.
   */

  sys_reboot(SYS_REBOOT_COLD);
}

int
main(void)
{
  const struct device *hw_wdt_dev = DEVICE_DT_GET(WDT_NODE);
  if (!device_is_ready(hw_wdt_dev)) {
    printk("Hardware watchdog %s is not ready; ignoring it.\n",
           hw_wdt_dev->name);
    hw_wdt_dev = NULL;
  }

  int ret = task_wdt_init(hw_wdt_dev);
  if (ret) {
    k_oops();
  }

  int wdt_id = task_wdt_add(10000, NULL, NULL);
  /*
   * Initialize the main work queue that can accept workers. This a
   * good practice instead of executing directly stuff into the timer.
   *
   * Timers on Zephyr operate in interrupt context.
   */
  k_work_queue_start(&main_work_q, workqueue_thread_stack,
                     K_THREAD_STACK_SIZEOF(workqueue_thread_stack), 5, NULL);
  k_work_init(&pwr_update, &pwr_update_worker);
  k_timer_init(&pwr_update_timer, &pwr_update_timer_handler, nullptr);

  LOG_MODULE_DECLARE(sidloc, CONFIG_LOG_DEFAULT_LEVEL);

  /*
   * NOTE: Singletons are using IO that may hang. It is quite important that
   *  the coresponding hardware is initialized after the watchdog activation
   */
  radio &rf = radio::get_instance();

  /* NOTE: Power should be initialized before the deployment procedure */
  auto &pwr = power::get_instance();
  auto &fp  = fpga::get_instance();
  auto &cnf = settings::get_instance();

  LOG_INF("successfully initialized");

  rf.enable(false);
  fp.enable(false);

  pwr.update();
  k_timer_start(&pwr_update_timer, K_MSEC(1000), K_MSEC(1000));

  auto &ant = antenna::get_instance();

  /*
   * If this is the first deploy, wait the configured time before proceeding to
   * the antenna deploy routine
   */
  if (ant.deployed() == false) {
    for (uint32_t i = 0; i < CONFIG_DEPLOY_HOLD_OFF_SECS; i++) {
      k_sleep(K_SECONDS(1));
      task_wdt_feed(wdt_id);
      LOG_INF("Deploy hold off %u/%u", i, CONFIG_DEPLOY_HOLD_OFF_SECS);
    }
  }

  /*
   * Go for antenna deploy!
   *
   * In case of reboot and previously succesfull deployment, the
   * deploy() method takes care of it and procceeeds without
   * activating again the thermal knives
   */
  ant.deploy(wdt_id);

  while (1) {
    auto       bat_status = pwr.get_status();
    const auto tx_ms      = cnf.tx_period();
    const auto rx_ms      = cnf.rx_period();
    auto       idle_ms    = cnf.idle_period();

    switch (bat_status) {
    /* In critical state, do nothing and disable any subsystem */
    case power::status::CRITICAL:
      LOG_INF("Bat status: CRITICAL");
      rf.enable(false);
      fp.enable(false);
      break;
    /* At low power, increase the duration of the IDLE period by a factor of 4
     */
    case power::status::LOW:
      idle_ms *= 4;
    case power::status::NORMAL: {
      if (bat_status == power::status::LOW) {
        LOG_INF("Bat status: LOW");
      } else {
        LOG_INF("Bat status: NORMAL");
      }
      ret = rf.enable();
      if (ret) {
        k_oops();
      }

      LOG_INF("Start RX");
      rf.set_frequency(CONFIG_SIDLOC_FREQ);
      rf.rx_async();
      for (size_t i = 0; i < rx_ms / 1000; i++) {
        task_wdt_feed(wdt_id);
        k_sleep(K_SECONDS(1));
        task_wdt_feed(wdt_id);
      }
      LOG_INF("Stop RX");

      LOG_INF("Start TX");
      if (cnf.tx_inhibit() == false) {
        /*
         * The FPGA boot-up sequence takes some time. So it for safety we update
         * the watchdog before and after.
         */
        task_wdt_feed(wdt_id);
        fp.enable();
        task_wdt_feed(wdt_id);

        /* Try 3 times to identify the FPGA */
        bool fpga_ok = false;
        for (size_t i = 0; i < 3; i++) {
          fpga_ok = fp.check();
          if (fpga_ok) {
            break;
          }
        }

        if (fpga_ok == false) {
          LOG_ERR("Could not communicate with the FPGA");
          k_oops();
        }

        rf.set_frequency(CONFIG_SIDLOC_FREQ);
        rf.iq_start(radio::direction::TX, AT86RF215_SR_2000KHZ);
        rf.set_gain(CONFIG_SIDLOC_AT86RF215_TX_PWR);
        fp.led(true);
      } else {
        LOG_INF("TX inhibit active");
      }

      for (size_t i = 0; i < tx_ms / 1000; i++) {
        task_wdt_feed(wdt_id);
        k_sleep(K_SECONDS(1));
        task_wdt_feed(wdt_id);
      }

      rf.iq_stop(radio::direction::TX);
      fp.enable(false);
      rf.enable(false);
      LOG_INF("Stop TX");

      LOG_INF("Start IDLE");
      for (size_t i = 0; i < idle_ms / 1000; i++) {
        task_wdt_feed(wdt_id);
        k_sleep(K_SECONDS(1));
        task_wdt_feed(wdt_id);
      }
      LOG_INF("Stop IDLE");
    } break;
    default:
      LOG_ERR("invalid battery status");
    }
    task_wdt_feed(wdt_id);
    k_msleep(1000);
  }
  return 0;
}

void
pwr_update_timer_handler(struct k_timer *t)
{
  k_work_submit_to_queue(&main_work_q, &pwr_update);
}

void
pwr_update_worker(struct k_work *item)
{
  power::get_instance().update();
}

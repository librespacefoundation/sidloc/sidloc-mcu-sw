/*
 *  SIDLOC MCU software: SIDLOC beacon software
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <bitset>

class settings
{
public:
  static constexpr bool     default_ant_deployed = false;
  static constexpr bool     default_tx_inhibit   = false;
  static constexpr uint32_t default_tx_period_ms = CONFIG_SIDLOC_TX_SECS * 1000;
  static constexpr uint32_t default_rx_period_ms = CONFIG_SIDLOC_RX_SECS * 1000;
  static constexpr uint32_t default_idle_period_ms =
      CONFIG_SIDLOC_IDLE_SECS * 1000;
  static constexpr const char *setting_antenna_deployed = "sidloc/ant_deployed";
  static constexpr const char *setting_tx_period        = "sidloc/tx_period";
  static constexpr const char *setting_rx_period        = "sidloc/rx_period";
  static constexpr const char *setting_idle_period      = "sidloc/idle_period";
  static constexpr const char *setting_tx_inhibit       = "sidloc/tx_inhibit";
  static constexpr const std::bitset<32> tx_inhibit_magic_val = {0xd3f3};

  static settings &
  get_instance()
  {
    static settings instance;
    return instance;
  }

  /* Singleton */
  settings(settings const &) = delete;

  void
  operator=(settings const &) = delete;

  bool
  antenna_deployed();

  void
  set_antenna_deployed(bool success);

  void
  set_tx_period(uint32_t ms);

  uint32_t
  tx_period();

  void
  set_rx_period(uint32_t ms);

  uint32_t
  rx_period();

  void
  set_idle_period(uint32_t ms);

  uint32_t
  idle_period();

  bool
  tx_inhibit();

  void
  set_tx_inhibit(bool yes);

protected:
  settings();
};

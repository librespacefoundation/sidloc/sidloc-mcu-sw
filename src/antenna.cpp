/*
 *  SIDLOC MCU software: SIDLOC beacon software
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "antenna.hpp"
#include "power.hpp"
#include "settings.hpp"
#include <zephyr/drivers/gpio.h>
#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>
#include <zephyr/task_wdt/task_wdt.h>

static const struct gpio_dt_spec gpio_antenna_deploy =
    GPIO_DT_SPEC_GET_BY_IDX(DT_NODELABEL(antenna_deploy), gpios, 0);
static const struct gpio_dt_spec gpio_antenna_sense =
    GPIO_DT_SPEC_GET_BY_IDX(DT_NODELABEL(antenna_sense), gpios, 0);

antenna::antenna()
    : m_dur_secs(CONFIG_SIDLOC_MAX_ANTENNA_DEPLOY_SECS),
      m_safe_margin_secs(CONFIG_SIDLOC_ANTENNA_POST_DEPLOY_SECS)
{
  gpio_pin_configure_dt(&gpio_antenna_deploy,
                        GPIO_OUTPUT_INACTIVE | gpio_antenna_deploy.dt_flags);
}

bool
antenna::sense()
{
  for (size_t i = 0; i < 50; i++) {
    m_dbounce.add(gpio_pin_get_dt(&gpio_antenna_sense));
    k_msleep(2);
  }
  return m_dbounce.is_held();
}

/**
 * @brief Deploys the antenna.
 *
 * This is a blocking method that can take longer than the task watchdog period.
 * For this reason it takes as parameter the task watchdog ID of the caller task
 * and updates it in regular intervals. It will check will make checks if the
 * antenna has been already deployed and if the antenna release switch can be
 * trusted.
 *
 * @param wdgid the task watchdog ID
 * @param bypass if set to yes, the deployment is performed even if the antenna
 * has been already deployed
 */
void
antenna::deploy(int wdgid, bool bypass)
{
  LOG_MODULE_DECLARE(sidloc, CONFIG_LOG_DEFAULT_LEVEL);
  if (deployed() && bypass == false) {
    LOG_INF("Antenna already deployed");
    return;
  }

  /*
   * If the switch is indicating that the antenna is released at the first try
   * continue with a time based approach
   */
  bool sw_ok = !sense();

  /*
   * Wait unitl the power reaches the normal state. If all hopes fade, do it
   * after one day
   */
  const auto &pwr = power::get_instance();
  uint32_t    cnt = 0;
  while (pwr.get_status() != power::status::NORMAL && cnt < 24 * 60 * 60) {
    k_msleep(1000);
    task_wdt_feed(wdgid);
    cnt++;
    LOG_INF("Waiting for the power to reach normal state");
  }

  /* Activate the thermal knives */
  gpio_pin_set_dt(&gpio_antenna_deploy, 1);
  LOG_INF("Thermal knives activated");
  uint32_t wait_period = m_dur_secs;
  for (uint32_t i = 0; i < wait_period; i++) {
    k_msleep(1000);
    task_wdt_feed(wdgid);
    if (sw_ok) {
      if (sense()) {
        LOG_INF("Antenna SW activated!");
        wait_period = i + m_safe_margin_secs;
        sw_ok       = false;
      }
    }
  }
  gpio_pin_set_dt(&gpio_antenna_deploy, 0);
  settings::get_instance().set_antenna_deployed(true);
  LOG_INF("Antenna deployed!");
}

bool
antenna::deployed()
{
  return settings::get_instance().antenna_deployed();
}

# SIDLOC MCU Software

## Development Guide
SIDLOC MCU software in independent of any kind of development tool.
You can use the development environment of your choice.

## Requirements
In general the host system should have at least:
- CMake (>= 3.20)
- C++17
- GNU Make or Ninja
- Zephyr SDK (>= 0.16)
- Zephyr-RTOS (>=4.0)
- Device Tree Compiler (dtc)
- clang-format (>= 17.0)

### Optional
Some optional tools that may also be usefull.

- openOCD (>= 0.11) (for testing and debugging)
- jlink (for testing and debugging)

### Dependencies
The SIDLOC MCU codebase depends on the 
- [MAX7261 Driver](https://gitlab.com/librespacefoundation/qubik/max17261-driver)
- [AT86RF215 Driver](https://gitlab.com/librespacefoundation/at86rf215-driver.git)
- [Embedded Template Library (ETL)](https://github.com/ETLCPP/etl.git)
 which is shipped as git submodule within the project.

## Build instructions

As the SIDLOC MCU firmware relies on the Zephyr-RTOS, most of the steps required to install
all the necessary requirements and compile the project can be found at the [Getting Started Guide](https://docs.zephyrproject.org/latest/develop/getting_started/index.html#getting-started-guide)
of the Zephyr-RTOS.

To build the SIDLOC firmware the following steps should be followed:


1. Clone the repository with all the necessary submodules
  ```bash
  git clone --recurse-submodules https://gitlab.com/librespacefoundation/sidloc/sidloc-mcu-sw.git
  ```

2. Enter the project's directory
  ```bash
  cd sidloc-mcu-sw
  ```

3. Fetch the `Zephyr-RTOS` codebase at the specific version associated with the project using the [west](https://docs.zephyrproject.org/latest/develop/west/index.html) manifest file shipped with the project
  ```bash
  west init --mf west.yml
  ```

4.  Build the firmware using the [west](https://docs.zephyrproject.org/latest/develop/west/index.html) tool.

For debugging purposes use the `debug.conf` file:
```bash
  west build --board=sidloc@1.0.0 -d build --pristine -o=-j$(nproc) -- -DEXTRA_CONF_FILE=boards/lsf/sidloc/debug.conf -DBOARD_ROOT=$PWD -DCONFIG_SPACECRAFT_ID=1
```
For releases use the `release.conf` file:
```bash
  west build --board=sidloc@1.0.0 -d build --pristine -o=-j$(nproc) -- -DEXTRA_CONF_FILE=boards/lsf/sidloc/release.conf -DBOARD_ROOT=$PWD -DCONFIG_SPACECRAFT_ID=1
```

## Flashing
The project provides flashing capabilities utilizing the `west flash` command.
Several runners are available including `jlink`, `stm32cubeprogremmer` and `openocd` covering the majority of the flashing probes.

For example:

```bash
west flash --runner jlink
```

## Website and Contact
For more information about the project and Libre Space Foundation please visit our [site](https://libre.space/)
and our [community forums](https://community.libre.space).

## License
![SIDLOC Logo](docs/assets/sidloc-logo-icon.png)
![Libre Space Foundation](docs/assets/LSF_HD_Horizontal_Color1-300x66.png)  
&copy; 2022 [Libre Space Foundation](https://libre.space).

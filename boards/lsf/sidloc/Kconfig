###############################################################################
#  SIDLOC MCU Software 
#
#  Copyright (C) 2024-2025, Libre Space Foundation <http://libre.space>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#  SPDX-License-Identifier: GNU General Public License v3.0 or later
###############################################################################

if BOARD_SIDLOC

config USE_TXCO
	bool "Select if the board uses a TXCO as the HSE"

config SIDLOC_FREQ
	int "SIDLOC Center frequency in Hz"

config SIDLOC_AT86RF215_TX_PWR
	int "The PAC.TXPWR register value for the TX gain"

# Ariane 6 specific. May be removed at the future
config SIDLOC_TX_SECS
	int "Number of seconds the SIDLOC beacon is transmitting"

config SIDLOC_RX_SECS
	int "Number of seconds the SIDLOC beacon is receiving"

config SIDLOC_RX_FRAME_LEN
	int "RX frame len including CRC"
	default 64

# Ariane 6 specific. May be removed at the future
config SIDLOC_IDLE_SECS
	int "Number of seconds the SIDLOC beacon is idle and not transmitting"

config SIDLOC_MAX_ANTENNA_DEPLOY_SECS
	int "The maximum time in seconds for the thermal knives"

config SIDLOC_ANTENNA_POST_DEPLOY_SECS
    int "The time in seconds, thermal knives remain active, after sensing that the antenna has been released"
    default 15

config DEPLOY_HOLD_OFF_SECS
    int "Deployement hold of period in seconds"
    default 2100

config TLC_SHA256_KEY
    string "Set the SHA256 of the uplink telecommands"
    default "00b0a7c2f63c426c59a10569183cab47ebb58a617d99ae6aa56f89fdd257b5c7"
    help
       This configuration accepts the SHA256 hash of the key that is used to athenticate
       the uplink telecommands. The hash should be given in hex form containing 64 characters.
       You can use directly the output of the sha256sum command

config SPACECRAFT_ID
	int "The spacecraft unique identifier"
	help
		Assign a unique ID at each spacecraft. Uplink telecommands share the same frequency
		so the spacecraft should only accept telecommands that are intended for it

endif
